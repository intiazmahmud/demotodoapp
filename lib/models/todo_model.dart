class TodoModel {
  int? id;
  DateTime? createdAt;
  String title = '';
  bool isCompleted = false;
  int priority = 1;

  TodoModel({
    required this.id,
    this.createdAt,
    required this.title,
    this.isCompleted = false,
    this.priority = 1,
  });

  TodoModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    createdAt = json['created_at'];
    title = json['title'] ?? '';
    isCompleted = json['is_completed'] ?? false;
  }

  TodoModel.copyFrom(TodoModel todoModel)
      : id = todoModel.id,
        priority = todoModel.priority,
        createdAt = todoModel.createdAt,
        isCompleted = todoModel.isCompleted,
        title = todoModel.title;

  static List<TodoModel> getTodoListFromJson(Map<String, dynamic> json) {
    List<TodoModel> list = [];

    for (var element in json['todos']['data']) {
      list.add(TodoModel.fromJson(element));
    }

    return list;
  }
}
