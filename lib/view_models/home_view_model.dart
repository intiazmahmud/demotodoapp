import 'dart:async';

import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:simple_todo_app/helpers/database_helper.dart';
import 'package:simple_todo_app/models/todo_model.dart';
import 'package:simple_todo_app/queries.dart';

class HomeViewModel with ChangeNotifier {
  GraphQLClient? client;
  DatabaseHelper helper = DatabaseHelper();
  List<TodoModel>? mainTodos;

  Future<String> getTodos() async {
    client ??= helper.getClient();
    Map<String, dynamic>? response;
    try {
      response = await helper.runQuery(client!, Queries.getAllTodosAscQuery);
    } catch (e) {
      return e.toString();
    }

    if (response == null) {
      return 'Query returned null';
    }
    mainTodos = [...TodoModel.getTodoListFromJson(response).reversed];
    notifyListeners();
    return 'Success';
  }

  Future<String> addTodo(TodoModel todoModel) async {
    client ??= helper.getClient();
    Map<String, dynamic> variables = {
      'is_completed': todoModel.isCompleted,
      'title': todoModel.title,
      // 'priority': todoModel.priority,
      // 'user': '07c42a78-d85f-46e8-a98b-2c3d45c3b52b',
    };

    Map<String, dynamic>? response;
    try {
      response = await helper.runMutation(client!, Queries.addQuery, variables);
    } catch (e) {
      return e.toString();
    }

    if (response == null) {
      return 'Mutation returned null';
    }
    TodoModel newModel = TodoModel.fromJson(response['createTodo']);
    newModel.createdAt = DateTime.now();
    mainTodos ??= [];
    // mainTodos!.add(newModel);
    mainTodos!.insert(0, newModel);
    notifyListeners();
    return 'Success';
  }

  Future<String> editTodo(TodoModel todoModel) async {
    client ??= helper.getClient();
    Map<String, dynamic> variables = {
      'id': todoModel.id,
      'is_completed': todoModel.isCompleted,
      // 'priority': todoModel.priority,
      'title': todoModel.title,
    };

    Map<String, dynamic>? response;
    try {
      response = await helper.runMutation(client!, Queries.editQuery, variables);
    } catch (e) {
      return e.toString();
    }

    if (response == null) {
      return 'Mutation returned null';
    }

    try {
      Map<String, dynamic> json = response['updateTodo'];
      TodoModel newModel = TodoModel.fromJson(json);
      mainTodos ??= [];
      mainTodos![mainTodos!.indexWhere((element) => todoModel.id == element.id)] = newModel;
      notifyListeners();
      return 'Success';
    } catch (e) {
      return 'Mutation returned null, condition may be wrong.';
    }
  }

  Future<String> deleteTodo(TodoModel todoModel) async {
    client ??= helper.getClient();
    Map<String, dynamic> variables = {
      'id': todoModel.id,
    };

    Map<String, dynamic>? response;
    try {
      response = await helper.runMutation(client!, Queries.deleteQuery, variables);
    } catch (e) {
      return e.toString();
    }

    if (response == null) {
      return 'Mutation returned null';
    }

    try {
      // Map<String, dynamic> json = response['deleteTodo'];
      mainTodos ??= [];
      mainTodos!.removeWhere((element) => todoModel.id == element.id);
      notifyListeners();
      return 'Success';
    } catch (e) {
      return 'Mutation returned null, condition may be wrong.';
    }
  }

  Future<bool> todoCardOnCheckedCallback(bool val, int todoIndex) async {
    TodoModel originalModel = mainTodos![todoIndex];
    TodoModel copy = TodoModel.copyFrom(originalModel);
    copy.isCompleted = val;
    try {
      String response = await editTodo(copy);
      if (response != 'Success') {
        return false;
      }
    } catch (e) {
      return false;
    }
    mainTodos![todoIndex] = copy;
    notifyListeners();
    return true;
  }
}
